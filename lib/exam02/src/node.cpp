#include "node.h"
// Take in value and create a node
node::node(int input)
{
    node *n;
        n=this;
        n->data=input;
        n->next= nullptr;

}
// Takes in an array of values and creates the ordered nodes
node::node(int values[], int length)
{
    this->data=values[0];
    node *tmp_head = this;
    for(int i=1; i<length; i++)
    {
        node *n = new node(values[i]);
        tmp_head->next = n;
        tmp_head=n;
    }
}

// Default destructor
node::~node()
{
    node* tmp_head=this;
    node *curr;
    if(tmp_head == nullptr)
    {
        return;
    }
    else
    {
        while(tmp_head != nullptr)
        {
            curr=tmp_head;
            tmp_head=curr->next;
            delete tmp_head;

        }
    }
}

// Insert a new node in appropriate location
node * node::insert(int value)
{
    // Must return head pointer location
    node *n = new node(value);
    n->data= value;
    node *curr=this;
    node *t = this;
    if(this == nullptr)
    {
        curr=n;
    }
    else if(curr->data>n->data)
    {
        n->next=curr;
        curr=n;
    }
    else
    {
        /*while(curr->next!= nullptr)
        {
            //if(curr->next->data < value) //check to see if node to be inserted is greater than prev node.
            {
                t=curr;
            }
            curr=curr->next;
        }*/
    }
}


// Remove the node if found value
node* node::remove(int value)
{
    // Must return head pointer location
    node *curr=this;
    if(this == nullptr)
    {
        return this;
    }
    else {
        while (curr != nullptr) {
            if (value == curr->data) {
                node *tmp = curr->next;
                curr->next=tmp;
                delete curr;
            }
            else
            {
                return this;
            }
            curr=curr->next;
        }
    }
}

// Print all nodes
void node::print()
{
    node *curr=this;
    while(curr!= nullptr) //execute as long as curr is not null
    {
        //std::cout<<curr->data; //print values, but need to include <iostream>
        curr=curr->next; //iterate through list
    }

}

// Get the value of a given node
int node::get_value(int location)
{
    node *curr=this;
    int count=0;
    while(curr!= nullptr)
    {
        if(count==location)
        {
            return curr->data;
        }
        curr=curr->next;
        count++;
    }

}
node* node::get_next()
{
    return next;
}
node *node::set_next(node *set)
{
    next=set;
}
int node::get_data()
{
    return data;
}
int node::set_data(int set)
{
    data=set;
}

